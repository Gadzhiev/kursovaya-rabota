﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        public static string element(string str)
        {
            string element=Convert.ToString(str[0]);
            bool flag=true;
            for (int i = 1; i < str.Length; i++)
            {
                flag = true;
                for (int j = 0; j < element.Length; j++)
                {
                    if (element[j] == str[i])
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                    element += Convert.ToString(str[i]);
            }
            return element;
        }
        public static string[] posledov(string[] p, string element, int k)
        {
            string[] posled = new string[element.Length*p.Length];
            int kol=0;
            for (int i = 0; i < element.Length; i++)
            {
                for (int j = 0; j < p.Length; j++)
                {
                    posled[kol] = element[i] + p[j];
                    kol++;
                }
            }
            if (k == 1)
                return posled;
            else
                return posledov(posled, element, k - 1);
        }
        public static void print(string[] p)
        {
            for (int i = 0; i < p.Length; i++)
                Console.WriteLine(p[i]);
        }
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            int n = int.Parse(Console.ReadLine());
            str = element(str);
            string []p=new string [str.Length];
            for (int i = 0; i < str.Length; i++)
                p[i] = Convert.ToString(str[i]);
            print(posledov(p,str,n-1));

        }
    }
}
