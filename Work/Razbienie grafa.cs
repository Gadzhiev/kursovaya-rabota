﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите количество вершин графа  ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите матрицу смежности");
            int[,] massiv = new int[n, n];
            for (int i = 0; i < n; i++)
            {
                string[] str = Console.ReadLine().Split(' ');
                for (int j=0; j<n; j++)
                    massiv[i, j] = int.Parse(str[j]);
            }
            for (int i = n / 2; i < n; i++)
                for (int j = 0; j < n / 2; j++)
                    if (massiv[i, j] == 1)
                    {
                        massiv[i, j] = 0;
                        massiv[j, i] = 0;
                    }
            Console.WriteLine(" матрица смежности разбитого графа ");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    Console.Write(massiv[i, j]+" ");
                Console.WriteLine();
            }
        }
    }
}
